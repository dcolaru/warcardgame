//
//  ViewController.swift
//  war Card Game
//
//  Created by iMac on 3/11/17.
//  Copyright © 2017 iMac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var rightImageView: UIImageView!
    @IBOutlet weak var reightScoreLabel: UILabel!
    var rightScore = 0
    
    @IBOutlet weak var leftScoreLabel: UILabel!
    @IBOutlet weak var leftImageView: UIImageView!
    var leftScore = 0
    
    let cardNames = ["card2","card3", "card4", "card5", "card6", "card7", "card8", "card9", "card10", "jack", "queen", "king","ace"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func dealTapped(sender:UIButton){
        //RAndomize left number from 0 to 12
        let leftNumber = Int(arc4random_uniform(13))
        leftImageView.image = UIImage(named: cardNames[leftNumber])
        
        //RAndomize left number from 0 to 12
        let rightNumber = Int(arc4random_uniform(13))
        rightImageView.image = UIImage(named:cardNames[rightNumber])
        //Compare the card numbers
        
        if leftNumber > rightNumber {
            // left card wins
            leftScore += 1
            
            leftScoreLabel.text = String(leftScore)
        }
        else if leftNumber < rightNumber {
            // right card wins
            rightScore += 1
            
            reightScoreLabel.text = String(rightScore)
            
        }
        


        
    }

}

